package com.webVillage.app;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.ArrayList;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class HomeWorkLecture30 {
    private WebDriver driver;
    private String startPage = "http://localhost:";

    @LocalServerPort
    private int port;

    @BeforeAll
    public static void setProperty() {
        System.setProperty("webdriver.chrome.driver",
                "src/drivers/chromedriver");
    }

    @BeforeEach
    public void beforeTest() {
        driver = new ChromeDriver();
    }

    @AfterEach
    public void afterTest() {
        driver.quit();
    }

    @Test
    void checkLinkJoeDow() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.get(startPage + port);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(., 'John Dow')]"))).click();
        ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(allTabs.get(1));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#nameLoaded")));
        String nameOfPerson1 = driver.findElement(By.cssSelector("#nameLoaded")).getText();
        Assertions.assertEquals("NAME: John Dow", nameOfPerson1);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "//h3;;Hello, World!",
            "//h3[2];;Find me!",
            "//h3[@class='header'];;And me!",
            "//h3[@class='header'][2];;NOW me!",
            "//h3[@class='header yellow'];;Do not forget about me",
            "//h3[@class='header'][@thestyle='red'];;It is simple",
            "//h3[contains(@class, 'abracadabra')][contains(@class, 'kitty-cat')];;My turn",
            "//h3[contains(@class, 'abracadabra')][contains(@class, 'bow-wow-dog')];;Where am I...",
            "//h3[not(contains(@class, 'abracadabra'))][contains(@class, 'kitty-cat')];;What about me",
            "//div[@id='good']/h3;;I am hiding",
            "//div[@id='bad']/h3;;U cannot get me!",
            "//div[@id='ugly']//h3;;Where is Blondie again?",
            "//p[@id='not_me'];;Find my daddy please.",
            "//p[@id='iAmLost']/ancestor::h3;;Look for my granny!",
            "//h3[@id='sister'];;Sister",
            "//h3[@id='sister']/../..//h3[not(@id='sister')];;Brother",
            "//div[@id='last']//h3[last()];;And me, please",
            "//div[@id='last']//h3[last()-1];;Find me!",
            "//h3[@price!=5];;Find me because my price is not 5",
            "//h3[@age>4];;Find me because my age is bigger than 4",
            "//h3[@courage>5 and @courage<15];;Find me because my courage is between 5 and 15",
            "//h3[text()='U can find me by text'];;U can find me by text",
            "//h3[contains(text(),'part of the text')];;U can find me by the part of the text"
    },
            delimiterString = ";;")
    void checkPopUp(String xpath, String expectedResult) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.get(startPage + port + "/person/1");
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#popupButton"))).click();
        ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(allTabs.get(1));
        WebElement element = driver.findElement(By.xpath(xpath));
        String text = element.getText();
        Assertions.assertEquals(expectedResult, text);
    }

    @Test
    void deleteJoeDow() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.get(startPage + port);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(., 'John Dow')]"))).click();
        ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(allTabs.get(1));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#nameLoaded")));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#deleteButton"))).click();
        driver.switchTo().alert().accept();
    }

    @Test
    void cookieTest() {
        driver.get(startPage + port);
        String valueCookieUser = driver.manage().getCookieNamed("user").getValue();
        Assertions.assertEquals(valueCookieUser, "Weyland-Yutani");
    }
}
